#ifndef __UTILS_INCLUDED__
#define __UTILS_INCLUDED__

#include <math.h>
#include <vector>
#include <iostream>
#include <chrono>
#include <limits>
#include <queue>
#include <tuple>

using namespace std;

struct Node {
    float x;
    float y;
    int id;
};

struct Edge {
    int nodeA;
    int nodeB;
    float weight;
};

struct Graph {
    int nodes; 
    vector<Edge> edges; // Una lista con los ejes
    vector<vector<float>> matrix; // La matriz para usar con prim. Va a estar vacía en los otros dos casos
};

struct Neighbor{
	int id;
	float weight;	
};

Graph armarGrafo();
Graph armarGrafoMatriz(); //prim

void imprimirAristas(vector<Edge> edges);
float pesoArbol(Graph& g);

#endif
