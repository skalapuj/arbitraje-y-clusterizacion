#include "utils.h"
#include "AGM-algorithms.h"

using namespace std;

void promediosAux(vector<Neighbor> adj[],int n,int &d, float &suma, int &nodos, bool visited[]){
    if (d == 0) return;

    visited[n] = true;
    
    for (int m = 0; m < adj[n].size(); m++){
        int next = adj[n][m].id;
        if(visited[next] == false){
          suma += adj[n][m].weight;
         
          nodos++;
          d--;

          promediosAux(adj,next,d,suma,nodos, visited);
        }
    }  
}

void calculoPromedios(Graph g, vector<float> average[], vector<Neighbor> adj[], int d_or) {
	for (int i = 0; i < g.nodes; i++) {
		vector<float> a(g.nodes,0);
        average[i] = a;

        for (int n = 0; n < adj[i].size(); n++) {
        	int d = d_or;
        	float suma = 0;
        	int nodos = 0;
        	int id = adj[i][n].id;
        	bool visited[g.nodes] = {0};
        	visited[id] = true;
        	promediosAux(adj, i, d, suma, nodos, visited);

        	if (nodos != 0 && suma !=0 ){
       			average[i][id]= suma / nodos;
       		}
    	}
	}
}

void desviosAux(vector<Neighbor> adj[],int n,int &d, float &suma, float averageN, int &nodos, bool visited[]){
    if (d == 0) return;

    visited[n] = true;
    
    for (int m = 0; m < adj[n].size(); m++){
        int next = adj[n][m].id;
        if(visited[next] == false){
          float dif = adj[n][m].weight - averageN;
          suma += pow(dif, 2);
          nodos++;
          d--;

          desviosAux(adj,next,d,suma, averageN, nodos, visited);
        }
    }  
}
void calculoDesvios(Graph g, vector<float> deviations[], vector<float> average[], vector<Neighbor> adj[], int d_or) {
	for (int i = 0; i < g.nodes; i++) {
		vector<float> a(g.nodes,0);
        deviations[i] = a;

        for (int n = 0; n < adj[i].size(); n++) {
        	int d = d_or;
        	float suma = 0;
        	int nodos = 0;
        	int id = adj[i][n].id;        	
        	float averageN = average[i][id];
        	bool visited[g.nodes] = {0};
        	visited[id] = true;
        	desviosAux(adj, i, d, suma, averageN, nodos, visited);

        	if (nodos - 1 != 0 && suma !=0 ){
       			deviations[i][id]= sqrt(suma / (nodos - 1));
       		}
    	}
	}
}

bool consistent(Edge edge, vector<Neighbor> adj[], float k, float t, vector<float> average[], vector<float> deviations[], bool c1, bool c2) {
  
  float averageA = average[edge.nodeA][edge.nodeB]; //promedio nodo A sin eje AB
  float averageB =  average[edge.nodeB][edge.nodeA]; //promedio nodo B sin eje AB

  float deviationA = deviations[edge.nodeA][edge.nodeB]; //desviacion nodo A sin eje AB
  float deviationB =  deviations[edge.nodeB][edge.nodeA]; //desviacion nodo B sin eje AB

  //primera definicion
  if(c1){  
    if (edge.weight > averageA + k*deviationA) {
      if (edge.weight > averageB + k*deviationB) return false;
    }
  }

  //segunda definicion
  if(c2){
    if((edge.weight / averageA) > t && (edge.weight / averageB) > t ){
      return false;
    }
  }

  return true;

}

void DFS(int v, int i, bool visited[], vector<int> &clusters, vector<Neighbor> adj[]) {
  visited[v] = true; 
    clusters[v] = i; 
  
    // recursion sobre los vecinos 
    for(int j = 0; j < adj[v].size(); j++) {
        int u = adj[v][j].id;
        if(!visited[u]) 
            DFS(u, i, visited, clusters, adj); 
    }
}

void armarClusters(vector<int> &clusters, vector<Neighbor> adj[], int n){
  bool *visited = new bool[n]; 
    for(int v = 0; v < n; v++) 
        visited[v] = false; 
  
  int i = 0; //contador de clusters

    for (int v=0; v<n; v++) 
    { 
        if (visited[v] == false) 
        { 
            // recorro todos los alcanzables y los marco como i
            DFS(v, i, visited, clusters, adj); 
  
            i++; 
        } 
    } 
}

int main(int argc, char *argv[]) {
  //parametros
  if (argc < 7){
    cout << "missing parameters:" << endl;
    cout << "cat instance | ./function k t d c1 c2 alg" << endl;
    return 1;
  }

  float k = atof(argv[1]);
  float t = atof(argv[2]);
  int d = atoi(argv[3]);
  bool c1 = ( atoi(argv[4]) == 0 ) ? false : true;
  bool c2 = ( atoi(argv[5]) == 0 ) ? false : true;
  int algorithm = atoi(argv[6]);

  auto start = chrono::steady_clock::now();

  Graph g;

  if(algorithm == 1) {   

    //input
    g = armarGrafoMatriz();
    //hacer AGM
    prim(g);

  } else if (algorithm == 2) {

    //input
    g = armarGrafo();
    //hacer AGM
    kruskal(g);

  } else {

    //input
    g = armarGrafo();
    //hacer AGM
    kruskalPC(g);
    
  }

  //armar lista de adyacencias
  vector<Neighbor> adj[g.nodes];
  for (Edge &edge : g.edges) {
    Neighbor a;
    Neighbor b;
    a.id = edge.nodeA;
    a.weight = edge.weight;
    b.id = edge.nodeB;
    b.weight = edge.weight;
    adj[a.id].push_back(b);
    adj[b.id].push_back(a);
  }

  //calculo el promedio
  vector<float> average[g.nodes];
  vector<float> deviations[g.nodes];
  
  calculoPromedios(g, average,adj, d);
  calculoDesvios(g, deviations,average, adj, d);

  //recorrer ejes y eliminar inconsistentes
  vector<Neighbor> consistent_adj[g.nodes];
  vector<Edge> consistent_edges;
  for (Edge edge : g.edges) {
    if (consistent(edge, adj, k, t, average, deviations, c1, c2)) {
      Neighbor a;
      Neighbor b;
      a.id = edge.nodeA;
      b.id = edge.nodeB;
      a.weight = edge.weight;
      b.weight = edge.weight;
      consistent_adj[a.id].push_back(b);
      consistent_adj[b.id].push_back(a);
      consistent_edges.push_back(edge);
    }
  }

  //armar lista de clusters
  vector<int> clusters(g.nodes, 0);
  armarClusters(clusters, consistent_adj, g.nodes);
  //imprimir el output
  for (int i = 0; i < g.nodes; i++) {
    //cout << clusters[i] << endl;
  }
 
  auto end = chrono::steady_clock::now();

  cout << chrono::duration_cast<chrono::nanoseconds>(end - start).count() << endl;

  return 0;
}
