#ifndef DISJOINT_SET_PC
#define DISJOINT_SET_PC

#include "utils.h"

using namespace std;

struct DisjointSetPC {
    
    vector<int> parents, size;

    int find(int i){
        return parents[i] == -1 ? i : (parents[i] = find(parents[i])); //con path compression
    }

    void unite(int i, int j) {
    i = find(i); j = find(j);
        if(i != j) {
            if(size[i] < size[j]) swap(i, j);
            size[i] += size[j];
            parents[j] = i;
        }
    }
};

DisjointSetPC armarDSPC(int n) {
    DisjointSetPC ds;
    ds.parents.assign(n, -1); ds.size.assign(n, 1);
    return ds;
}

#endif //DISJOINT_SET
