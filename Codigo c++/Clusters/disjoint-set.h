#ifndef DISJOINT_SET
#define DISJOINT_SET

#include "utils.h"

using namespace std;

struct DisjointSet {
    
    vector<int> parents, size;

    int find(int i){
        return parents[i] == -1 ? i : find(parents[i]);
    }

    void unite(int i, int j) {
    i = find(i); j = find(j);
        if(i != j) {
            if(size[i] < size[j]) swap(i, j);
            size[i] += size[j];
            parents[j] = i;
        }
    }
};

DisjointSet armarDS(int n) {
    DisjointSet ds;
    ds.parents.assign(n, -1); ds.size.assign(n, 1);
    return ds;
}

#endif //DISJOINT_SET
