#include "utils.h"

using namespace std;

Graph armarGrafo(){
	int n;
	cin >> n;

    Graph g;
    g.nodes=n;

	vector<Edge> edges;
    vector<Node> nodes;

    float x,y;
	
    for (int i = 0; i < n; i++)
    {
        cin >> x;
        cin >> y;

        Node n;
        n.x=x;
        n.y=y;
        n.id=i;

        nodes.push_back(n);
    }
    
	for( int i = 0; i < nodes.size(); i++){

        for( int j = i+1; j < nodes.size(); j++){
            
                float distancia;
                Edge e;
                
                float dist = pow(nodes[i].x - nodes[j].x,2) + pow(nodes[i].y - nodes[j].y,2);
                distancia = sqrt(dist);
                

                e.nodeA=nodes[i].id;
                e.nodeB=nodes[j].id;
                e.weight=distancia;
                
                
                edges.push_back(e);
            
        }
	}

    g.edges=edges;
	return g;
};

Graph armarGrafoMatriz(){
	int n;
	cin >> n;

    Graph g;
    g.nodes=n;

	vector<vector<float>> matrix (n,vector<float>(n));
    vector<Node> nodes;

    float x,y;
	
    for (int i = 0; i < n; i++)
    {
        cin >> x;
        cin >> y;

        Node n;
        n.x=x;
        n.y=y;
        n.id=i;

        nodes.push_back(n);
    }
    
	for( int i = 0; i < nodes.size(); i++){
        for( int j = i+1; j < nodes.size(); j++){
            
                float distancia;
                
                float dist = pow(nodes[i].x - nodes[j].x,2) + pow(nodes[i].y - nodes[j].y,2);
                distancia = sqrt(dist);

                int a=nodes[i].id;
                int b=nodes[j].id;

                
                matrix[a][b] =distancia;
                matrix[b][a] =distancia;

        }
	}

    g.matrix=matrix;
	return g;
};