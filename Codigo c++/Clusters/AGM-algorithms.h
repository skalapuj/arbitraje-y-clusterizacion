#ifndef __AGM_INCLUDED__
#define __AGM_INCLUDED__

#include "utils.h"
#include "disjoint-set.h"
#include "ds-path-compression.h"

using namespace std;

void merge(vector<Edge>& _edges, int l, int m, int r) 
{ 
    int i, j, k; 
    int n1 = m - l + 1; 
    int n2 =  r - m; 
  
    //subarrays temporales
    vector<Edge> L, R;
    for (i = 0; i < n1; i++) 
        L.push_back(_edges[l + i]); 
    for (j = 0; j < n2; j++) 
        R.push_back(_edges[m + 1+ j]); 
  
    //merge
    i = 0; // index del primer subarray
    j = 0; // index del segundo subarray
    k = l; // index del array final 
    while (i < n1 && j < n2) 
    { 
        if (L[i].weight <= R[j].weight) 
        { 
            _edges[k] = L[i]; 
            i++; 
        } 
        else
        { 
            _edges[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 
  
    //agregar elementos restantes
    while (i < n1) 
    { 
        _edges[k] = L[i]; 
        i++; 
        k++; 
    }
    while (j < n2) 
    { 
        _edges[k] = R[j]; 
        j++; 
        k++; 
    } 
} 

void mergeSort(vector<Edge>& _edges, int l, int r) 
{ 
    if (l < r) 
    { 
        // calcular indice del medio
        int m = l+(r-l)/2; 
  
        // ordenar ambas mitades 
        mergeSort(_edges, l, m); 
        mergeSort(_edges, m+1, r); 
        
        //merge
        merge(_edges, l, m, r); 
    } 
}

void kruskal(Graph& g) {
  //sort inicial
  vector<Edge> _edges = g.edges;
  mergeSort(_edges, 0, _edges.size() - 1);
  vector<Edge> res;
  //algoritmo de Kruskal;
  DisjointSet ds = armarDS(g.nodes);

  for(int i = 0, j = 0; i < g.nodes-1; ++i) {
      
    while(ds.find(_edges[j].nodeA) == ds.find(_edges[j].nodeB)) ++j;
    ds.unite(_edges[j].nodeA,_edges[j].nodeB);
    res.push_back(_edges[j]);
  }

  g.edges = res;
}

void kruskalPC(Graph& g) {
  //sort inicial
  mergeSort(g.edges, 0, g.edges.size() - 1);
  vector<Edge> res;
  //algoritmo de Kruskal;
  DisjointSetPC ds = armarDSPC(g.nodes);

  for(int i = 0, j = 0; i < g.nodes-1; ++i) {
      
    while(ds.find(g.edges[j].nodeA) == ds.find(g.edges[j].nodeB)) ++j;
    ds.unite(g.edges[j].nodeA,g.edges[j].nodeB);
    res.push_back(g.edges[j]);
  }

  g.edges = res;
}


//PRIM
const int INT_MAX = numeric_limits<int>::max();

int minKey(float key[], bool visited[], int V)  
{  
    // inicializar min para comparar 
    float min = INT_MAX, min_index;  
  
    for (int v = 0; v < V; v++)  
        if (visited[v] == false && key[v] < min)  
            min = key[v], min_index = v;  
  
    return min_index;  
}  
  
void prim(Graph& g)  
{  
    int V = g.nodes; 
    int parent[V];  
      
    // key va a ser la distancia de cada nodo al arbol  
    float key[V];  

    bool visited[V];  
  
    // todas las keys empiezan como infinito
    for (int i = 0; i < V; i++)  
        key[i] = INT_MAX, visited[i] = false;  
  
    // Para que el vertice 0 sea elegido primero  
    key[0] = 0;  
    parent[0] = -1; // no tiene padre 
  
    // queremos agregar v - 1 aristas  
    for (int count = 0; count < V - 1; count++) 
    {  
        // Se elige el que este a menor distancia 
        int u = minKey(key, visited, V);  
  
        // se lo agrega al AGM 
        visited[u] = true;  
  
        // actualizar padres y distancias  
        for (int v = 0; v < V; v++)  
  
            // solo consideramos los adyacentes al que agregamos antes  
            if (g.matrix[u][v] && visited[v] == false && g.matrix[u][v] < key[v])  
                parent[v] = u, key[v] = g.matrix[u][v];  
    }

    vector<Edge> edges;
    //output del algoritmo
    for(int i = 1; i < V; ++i){
        Edge e;
        e.nodeA=i;
        e.nodeB=parent[i];
        e.weight= g.matrix[i][parent[i]];

        edges.push_back(e);
    }

    g.edges=edges;
    vector<vector<float>> empty;
    g.matrix=empty;

}

#endif