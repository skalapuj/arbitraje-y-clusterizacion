#ifndef TP_II_GRAFO_H
#define TP_II_GRAFO_H

#include <vector>
using namespace std;

class  Grafo{
public:
    Grafo();
    int cantidadDivisas();
    vector<vector<float>> divisas();
    vector<vector<float>> grafo();

private:
    int _cantidadDivisas;
    vector<vector<float>> _divisas;
    vector<vector<float>> _sinConversion;

};

#endif //TP_II_GRAFO_H