#include <iostream>
#include "Grafo.h"
#include <tgmath.h>

using namespace std;

Grafo::Grafo() {
    int cantidadDivisas;
    cin >> cantidadDivisas;
    _cantidadDivisas = cantidadDivisas;

    vector<vector<float>> divisas;
    vector<vector<float>> divisasSinConvertir;
    float conversion;
    for (int j = 0; j < cantidadDivisas; ++j) {
        vector<float> conversionDeDivisaI;
        vector<float> DivisaI;
        for (int i = 0; i < cantidadDivisas; ++i) {
            cin >> conversion;
            if (conversion > 0) { conversionDeDivisaI.push_back(-1 * log(conversion)); }
            else {
                conversionDeDivisaI.push_back(-1 * log(-1 * conversion));
            }
            DivisaI.push_back(conversion);
        }
        divisas.push_back(conversionDeDivisaI);
        divisasSinConvertir.push_back(DivisaI);
    }

    _sinConversion = divisasSinConvertir;
    _divisas = divisas;
}

vector<vector<float>> Grafo::grafo(){
    return _sinConversion;
}

int Grafo::cantidadDivisas() {
    return _cantidadDivisas;
}

vector<vector<float>> Grafo::divisas() {
    return _divisas;
}