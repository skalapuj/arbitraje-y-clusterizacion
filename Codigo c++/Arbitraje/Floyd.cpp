#include <iostream>
#include "Grafo.h"
#include <tuple>
#include <vector>

using namespace std;

vector<float> caminoMinimoFloyd(int v, vector<vector<float>> & predecesor){
    int pred = predecesor[v][v];
    int abuelo = v;
    vector<float> res;
    res.push_back(v);
    while (pred != v) {
        res.push_back(pred);
        if(predecesor[pred][v] == abuelo){
            break;
        }
        abuelo = pred;
        pred = predecesor[pred][v];
    }
    res.push_back(v);
    return res;
}


tuple<int,vector<float>> floydWarshall (Grafo &grafo){
    int V = grafo.cantidadDivisas();
    vector<vector<float>> dist=grafo.divisas();
    vector<vector<float>> predecesor=grafo.divisas();

    int i, j, k;
    int m=-1; //nodo que voy a usar si hay arbitraje. Si al finalizar está en -1 es que no hubo arbitraje.


    for (i = 0; i < V; i++)
        for (j = 0; j < V; j++)
            predecesor[i][j] = j;


    for (k = 0; k < V; k++)
    {
        if(m==-1){
            // Pick all vertices as source one by one
            for (i = 0; i < V; i++)
            {
                if(m==-1){
                    // Pick all vertices as destination for the
                    // above picked source
                    for (j = 0; j < V; j++)
                    {
                        if(m==-1){
                            // If vertex k is on the shortest path from
                            // i to j, then update the value of dist[i][j]
                            if (dist[i][k] + dist[k][j] < dist[i][j] && k!=i){
                                float a = dist[i][j];
                                dist[i][j] = dist[i][k] + dist[k][j];

                                predecesor[i][j] = predecesor[i][k];


                                if(i==j && dist[i][j]<0){

                                    //hay arbitraje partiendo desde i
                                    //cout << i << ": " << a << " replaced with "<< dist[i][j] << endl;
                                    //cout << "hay arbitraje" << endl;
                                    m=i;
                                    //esto se podría cortar acá, pero no confío en el break
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if(m!=-1){
        return make_tuple(1,caminoMinimoFloyd(m, predecesor));
    }else{
        vector<float> vacio;
        return make_tuple(0,vacio);
    }
}

float beneficio(vector<float> camino, Grafo g){
    float res=1;
    int desde=0;
    int hasta=1;
    for (int i = 0; i < camino.size() - 1 ; ++i) {
        res= res * g.grafo()[camino[desde]][camino[hasta]];
        desde=hasta;
        hasta=desde+1;
    }
    return res-1;
}

int main() {
    Grafo grafo;
    vector<float> vacio;
    tuple<int,vector<float>> resultadoTemporal= make_tuple(0,vacio);
    resultadoTemporal = floydWarshall(grafo);
    if(get<0>(resultadoTemporal)==0){
        cout << "NO";
    }else{
        cout << "SI" <<' ' << '<';
        for (int k = 0; k < get<1>(resultadoTemporal).size(); ++k) {
            if(k == get<1>(resultadoTemporal).size() - 1 ){
                cout << get<1>(resultadoTemporal)[k];
            }else{
                cout << get<1>(resultadoTemporal)[k] << ',';
            }

        }
        cout << '>';
    }
    cout << endl;
    return 0;
}

