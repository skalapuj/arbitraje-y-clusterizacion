
#include <iostream>
#include "Grafo.h"
#include <tuple>
#include <tgmath.h>
#include <iostream>
#include "Grafo.h"
#include <tuple>
#include <tgmath.h>
#include <cstring>
#include <vector>
#include <chrono>

using namespace std;

float suma(float p, float q) {
    if (p == INFINITY || q == INFINITY) {
        return INFINITY;
    } else {
        return p + q;
    }
}


vector<float> caminoMinimoFord(int v, vector<float> &predecesor) {
    int pred = predecesor[v];
    int abuelo = v;
    vector<float> res;
    res.push_back(v);
    while (pred != v) {
        res.push_back(pred);
        if (predecesor[pred] == abuelo) {
            break;
        }
        abuelo = pred;
        pred = predecesor[pred];
    }
    res.push_back(v);
    return res;
}


tuple<int, vector<float>> bellmanFord(int divisa, Grafo &G) {
    int n = G.cantidadDivisas();

    vector<float> dist(n, INFINITY), spTree(n, divisa), modified(n, false);
    vector<vector<float>> conversionDivisas = G.divisas();
    dist[divisa] = 0;
    bool changed = true;
    modified[divisa] = true;
    int end = -1;

    for (int i = 0; i <= n and changed; ++i) {
        changed = false;

        for (int v = 0; v < n; ++v) {
            if (modified[v]) {
                modified[v] = false;

                for (int m = 0; m < n and end == -1; ++m) //si end deja de ser -1
                {
                    if (dist[v] + conversionDivisas[v][m] < dist[m]) {
                        modified[m] = changed = true;
                        dist[m] = dist[v] + conversionDivisas[v][m];
                        spTree[m] = v;

                        if (m == divisa) {
                            end = v;
                            changed = false;//seteo changed = false porque ya encontré un ciclo negativo
                        }

                    }
                }
            }
        }
    }

    if (dist[divisa] < 0) {
        return make_tuple(1, caminoMinimoFord(divisa, spTree));
    } else {
        vector<float> vacio;
        return make_tuple(0, vacio);
    }
}

void ejecuctarFord(tuple<int, vector<float>> &resultadoTemporal, Grafo g) {
    for (int i = 0; i < g.cantidadDivisas() and get<0>(resultadoTemporal) == 0; ++i) { // 0 es no hay arbitraje
        resultadoTemporal = bellmanFord(i, g);
    }
}

float beneficio(vector<float> camino, Grafo g){
    float res=1;
    int desde=0;
    int hasta=1;
    for (int i = 0; i < camino.size() - 1 ; ++i) {
        res= res * g.grafo()[camino[desde]][camino[hasta]];
        desde=hasta;
        hasta=desde+1;
    }
    return res-1;
}

int main() {
    Grafo grafo;
    vector<float> vacio;
    tuple<int,vector<float>> resultadoTemporal= make_tuple(0,vacio);
    for (int i = 0; i < grafo.cantidadDivisas() and get<0>(resultadoTemporal)==0; ++i) {
        resultadoTemporal = bellmanFord(i,grafo);
    }
    if(get<0>(resultadoTemporal)==0){
        cout << "NO";
    }else{
        cout << "SI" <<' ' << '<';
        for (int k = 0; k < get<1>(resultadoTemporal).size(); ++k) {
            if(k == get<1>(resultadoTemporal).size() - 1 ){
                cout << get<1>(resultadoTemporal)[k];
            }else{
                cout << get<1>(resultadoTemporal)[k] << ',';
            }

        }
        cout << '>';
    }
    cout << endl;
    return 0;
}