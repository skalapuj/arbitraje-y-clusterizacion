import numpy as np
import math
import sys
import datetime
import random


def createInstance(i,cantInstancias):
    cantItems = i
    if len(sys.argv) > 1:
        cantItems = int(sys.argv[1])

    # creates file_name
    currentDT = datetime.datetime.now()
    h = (currentDT.strftime("%H"))
    m = (currentDT.strftime("%M"))
    s = (currentDT.strftime("%S"))
    ms = currentDT.microsecond

    file_name = './instancia_random_%s_%s_%s_%s.txt' % (h, m, s, ms)
    output_file = open(file_name, 'w+')

    max = 100
    print(int(cantItems))
    output_file.write(str(int(cantItems)) + '\n')

    Matrix = [[0 for i in range(int(cantItems))] for j in range(int(cantItems))]

    for i in range(0, int(cantItems)):
        for j in range(i, int(cantItems)):
            if i == j:
                Matrix[i][j] = 1
            # output_file.write(str(1) + '\n') 
            else:  ##cantItems*i + j
                Matrix[i][j] = 1
                Matrix[j][i] = 1
            # los voy imprimiendo en el archivo de salida
            # output_file.write(str(c) + '\n')       

    for i in range(0, int(cantItems)):
        for j in range(0, int(cantItems)):
            output_file.write(str(Matrix[i][j]) + '\n')

    output_file.close()


def main():
    if len(sys.argv) > 2 and str(sys.argv[1]) == 'help':
        print(
            "Uso: python generar_instancias_fb.py \n no necesita params: genera instancias para fuerza bruta.\n 42 instancias random\n las mismas 42 con w=1 \n las mismas 42 con w=10000")
        exit(1)

    cantInstancias = 100
    if len(sys.argv) > 3:
        cantInstancias = int(sys.argv[3])

    for i in range(0, cantInstancias):
        createInstance(i,cantInstancias)


if __name__ == "__main__":
    main()

